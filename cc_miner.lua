term.clear()
local length = tonumber(arg[1])
local width = tonumber(arg[2])
local depth = tonumber(arg[3])
local startDepth = tonumber(arg[4])

if(type(length) ~= "number" or length < 1) then
    print("Length (argument 1) must be a number and greater than 0")
    return
end

if(type(width) ~= "number" or width < 1) then
    print("Width (argument 2) must be a number and greater than 0")
    return
end

if(type(depth) ~= "number" or depth < 1) then
    print("Depth (argument 3) must be a number and greater than 0")
    return
end

print("Creating a quarry L:W:D - "..length..":"..width..":"..depth)

local startFuel = turtle.getFuelLevel()
local currentFuel = startFuel
local returning = false
local isHome = true

local x = 0
local y = 0
local z = 0
local facing = 0
--0 = +x
--1 = +y
--2 = -x
--3 = -y

local lastX = x
local lastY = y
local lastZ = z
local lastFacing = facing

function goForward(distance)
    for i=1, distance, 1 do
        currentFuel = turtle.getFuelLevel()
        if(returning ~= true and startFuel - currentFuel > currentFuel - 2) then
            lastX = x
            lastY = y
            lastZ = z
            lastFacing = facing
            goGetMoreFuel(false)
            depositMined(true)
        end
        if(returning ~= true and checkHasSpace() ~= true) then
            lastX = x
            lastY = y
            lastZ = z
            lastFacing = facing
            depositMined(false)
            goGetMoreFuel(true)
        end
        
        local hasBlock, data = turtle.inspect()
        if(hasBlock and data.name == "minecraft:bedrock") then
            print("Hit bedrock, returning")
            goUp(1)
            return false
        end
        turtle.dig()
        turtle.forward()
        isHome = false
        if (facing == 0) then
            x = x + 1
        elseif (facing == 1) then
            y = y + 1
        elseif (facing == 2) then
            x = x - 1
        elseif (facing == 3) then
            y = y - 1
        end
    end
    return true
end

function checkHasSpace(blockType)
    local space = false
    for i=1, 16, 1 do
        if(turtle.getItemSpace(i) == 64) then
            space = true
        end
    end
    return space
end

function goBack(distance)
    for i=1, distance, 1 do
        turtle.back()
    end
end

function goDown(distance)
    for i=1, distance, 1 do
        turtle.digDown()
        turtle.down()
        z = z + 1
    end
end

function turn(direction, count)
    for i=1, count, 1 do
        if(direction == 0) then
            turtle.turnLeft()
            if (facing == 0) then
                facing = 3
            elseif (facing == 1) then
                facing = 0
            elseif (facing == 2) then
                facing = 1
            elseif (facing == 3) then
                facing = 2
            end
        else
            turtle.turnRight()
            if (facing == 0) then
                facing = 1
            elseif (facing == 1) then
                facing = 2
            elseif (facing == 2) then
                facing = 3
            elseif (facing == 3) then
                facing = 0
            end
        end
    end
end

function goUp(distance)
    for i=1, distance, 1 do
        turtle.up()
        z = z - 1
    end
end

function goHome()
    print("Returning to home")
    goUp(z)
    returning = true
    if (facing == 0) then
        turn(0, 1)
    elseif (facing == 1) then
        turn(0, 2)
    elseif (facing == 2) then
        turn(1, 1)
    end

    goForward(y)

    if (facing == 1) then
        turn(1, 1)
    elseif (facing == 2) then
        turn(1, 2)
    elseif (facing == 3) then
        turn(0, 1)
    end

    goForward(x)

    turn(1,2)
    isHome = true
    returning = false
    print("Home!")
end

function depositMined(continue)
    if(isHome == false) then
        
        goHome()
    end 
    turn(0,1)
    for i=1, 16, 1 do
        turtle.select(i)
        turtle.drop()
    end
    turn(1,1)
    if(continue) then
        goToCoords(lastX, lastY, lastZ, lastFacing)
    end
end

function goGetMoreFuel(continue)
    if(isHome == false) then
        goHome()
    end 
    turn(1,2)
    local slot = 1
    turtle.select(slot)
    repeat until(turtle.suck() ~= true)
    repeat 
        turtle.select(slot)
        slot = slot + 1
        local refueled = turtle.refuel()
        turtle.drop()
    until(refueled ~= true or slot > 16)
    turn(1,2)
    
    if(continue) then
        goToCoords(lastX, lastY, lastZ, lastFacing)
    end
end

function goToCoords(xCoord, yCoord, zCoord, faceDir)
    goForward(xCoord)
    turn(1, 1)
    goForward(yCoord)
    turn(0, 1)
    goDown(zCoord)
    if(facing ~= faceDir) then
        repeat
            turn(1, 1)
        until(facing == faceDir)
    end
end

function startMining()
    local direction = 1

    goForward(1)
    if(type(startDept) == "number") then 
        goDown(startDepth)
    end

    for  d = 1, depth, 1 do
        for  w = 1, width, 1 do
            if(goForward(length-1) ~= true) then
                return
            end
            if(w < width) then
                if(direction == 0) then
                    turn(direction, 1)
                    goForward(1)
                    turn(direction, 1)
                    direction = 1
                else
                    turn(direction, 1)
                    goForward(1)
                    turn(direction, 1)
                    direction = 0
                end
            end
        end
        if(direction == 0) then
            turn(0, 2)
        else 
            turn(1, 2)
        end

        if(d < depth) then
            local hasBlockBelow, dataBelow = turtle.inspect()
            if(hasBlock and data.name == "minecraft:bedrock") then
                print("Hit bedrock, returning")
                return
            end
            goDown(1)
        end
    end
end

startMining()
depositMined(false)
goGetMoreFuel(false)
